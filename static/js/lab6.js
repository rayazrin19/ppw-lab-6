$(window).on('load', function() {
    // Animate loader off screen
    setTimeout(function() {
        $(".se-pre-con").fadeOut("slow");
    }, 1500)

});


$(function() {
    $("#accordion").accordion();
});

$(function() {
    var toggle = true
    $("#id_change").on('click', function() {
        if (toggle) {
            $("body").css({
                'background': "url('https://wallpapercave.com/wp/wp3085091.jpg')",
            })
            $("h1").css('color', 'white')
            $("#favH1").css('color', 'white')
            $("h5").css('color', 'white')
            $("#id_change").removeClass("btn-dark")
            $("#id_change").addClass("btn-light")
            $(".btn-primary,rounded").addClass("btn-dark")
            $(".btn-primary,rounded,btn-dark").removeClass("btn-primary")
            $(".table").addClass("table-dark")
            $(".table").addClass("table-striped")
            $("p").css('color', 'black')
            $("ul").css('color', 'black')
            toggle = false
        } else {
            $("body").css({
                'background': "url('https://blog.visme.co/wp-content/uploads/2017/07/50-Beautiful-and-Minimalist-Presentation-Backgrounds-031.jpg')",
                'color': 'black',
            })
            $("#favH1").css('color', 'black')

            $("#id_change").removeClass("btn-light")
            $("#id_change").addClass("btn-dark")
            $(".btn-dark,rounded").addClass("btn-primary")
            $(".btn-dark,rounded,btn-primary").removeClass("btn-dark")
            $(".table,table-striped,table-dark").removeClass("table-dark")
            $(".table,table-striped,table-dark").removeClass("table-striped")

            toggle = true
        }
    });
});