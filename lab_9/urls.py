from django.urls import path

from . import views
app_name = 'lab_9'
urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('private/api/<str:search>', views.getJSON, name='books'),
    path('private/api/session/like/', views.like, name='like'),
    path('private/api/session/like/get/', views.get_like, name='get_like'),
    path('private/api/session/unlike/', views.unlike, name='unlike'),
]
