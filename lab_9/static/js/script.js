var counter = 0;

function find(search) {
    $.ajax({
        type: 'GET',
        url: '/book/private/api/' + search,
        dataType: 'json',
        success: function(data) {
            $('tbody').empty();
            var print = '<tr>';
            for (var i = 0; i < data.data.length; i++) {
                print += '<td>' + (i + 1) + '</td>'
                print += '<td><img src="' + data.data[i].thumbnail + '"></td>'
                print += '<td>' + data.data[i].title + '</td>';
                print += '<td>' + data.data[i].author + '</td>';
                print += '<td>' + data.data[i].publishedDate + '</td>';
                print += ' <td><i name = "' + data.data[i].id + '"class ="fav far fa-star"></i> </td></tr>';
            }
            $('tbody').append(print);
            checkLike();
        }
    });
}

function search() {
    var search = $('input[name=keyword]').val()
    $('.counter').html(counter);

    find(search)
}

$(document).ready(function() {
    find('quilting')
    $(document).on('click', '.fav', function() {
        var id = $(this).attr("name");
        var ini = $(this)
        if (ini.hasClass('clicked')) {
            $.ajax({
                url: "/book/private/api/session/unlike/",
                type: "POST",
                data: {
                    id: id,
                },
                success: function(result) {
                    counter = result.message;
                    ini.attr('class', 'fav far fa-star');
                    $('.counter').html(counter);
                },
                error: function(errmsg) {
                    alert("Something is wrong");
                }
            });

        } else {
            $.ajax({
                url: "/book/private/api/session/like/",
                type: "POST",
                data: {
                    id: id,
                },
                success: function(result) {
                    counter = result.message;
                    ini.attr('class', 'clicked fav fas fa-star');
                    $('.counter').html(counter);
                },
                error: function(errmsg) {
                    alert("Something is wrong");
                }
            });
        }
    });
});

function checkLike() {
    $.ajax({
        type: 'GET',
        url: '/book/private/api/session/like/get',
        dataType: 'json',
        success: function(data) {
            for (var i = 1; i <= data.message.length; i++) {
                var id = data.message[i - 1];
                var td = document.getElementsByName(id)[0];
                if (typeof td !== 'undefined') {
                    td.className = 'fas fa-star fav clicked';
                }
                $('.counter').html(data.message.length);
            }
        }
    });
};