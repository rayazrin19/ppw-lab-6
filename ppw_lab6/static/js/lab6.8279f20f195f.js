$(window).load(function() {
		// Animate loader off screen
        setTimeout(function(){$(".se-pre-con").fadeOut("slow");}, 1500)

	});


$(function () {
    $("#accordion").accordion();
});

$(function () {
    var toggle = true
    $("#id_change").on('click', function(){
        if(toggle){
            $("body").css({
                'background' : "url('https://wallpapercave.com/wp/wp3085091.jpg')",
                'color' : 'white',
            })
            $("#id_change").removeClass("btn-dark")
            $("#id_change").addClass("btn-light")
            $(".btn-primary,rounded").addClass("btn-dark")
            $(".btn-primary,rounded,btn-dark").removeClass("btn-primary")
            $("p").css('color', 'black')
            $("ul").css('color', 'black')
            toggle = false
        }
        else{
            $("body").css({
                'background' : "url('https://blog.visme.co/wp-content/uploads/2017/07/50-Beautiful-and-Minimalist-Presentation-Backgrounds-031.jpg')",
                'color' : 'black',
            })
            $("#id_change").removeClass("btn-light")
            $("#id_change").addClass("btn-dark")
            $(".btn-dark,rounded").addClass("btn-primary")
            $(".btn-dark,rounded,btn-primary").removeClass("btn-dark")
            toggle = true
        }
    });
});
