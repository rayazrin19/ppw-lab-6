from django.urls import path
from . import views
app_name = 'lab_10'

urlpatterns = [
    path('', views.index, name='index'),
    path('subscribe/', views.subscribe, name='subscribe'),
    path('validate_email/', views.validate, name='validate')
]
