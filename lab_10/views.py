from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.validators import validate_email
from .forms import SubscriberForm
from .models import Subscriber
# Create your views here.

response = {}


@csrf_exempt
def validate(request):
    if request.method == 'POST':
        email = request.POST['email']
        name = request.POST['name']
        password = request.POST['password']
        subs_filter = Subscriber.objects.filter(email=email)

        try:
            validate_email(email)
        except Exception:
            return JsonResponse({'message': 'Email is invalid'})
        if len(subs_filter) > 0:
            return JsonResponse({'message': 'Email is already subscribed'})
        if len(name) == 0 or len(name) > 32:
            return JsonResponse({'message': 'Name is not valid'})
        if len(password) < 8:
            return JsonResponse({'message': 'Password is not valid'})
        else:
            return JsonResponse({'message': 'All fields are valid'})
    else:
        return JsonResponse({'message': 'Something is wrong'})


@csrf_exempt
def subscribe(request):
    if request.method == "POST":
        response['form'] = SubscriberForm(request.POST or None)
        email = request.POST['email']
        name = request.POST['name']
        password = request.POST['password']
        subscriber = Subscriber.objects.filter(email=email)
        if len(subscriber) == 0:
            sub = Subscriber(email=email, name=name, password=password)
            sub.save()
    else:
        response['form'] = SubscriberForm()
    return render(request, 'subscribe.html', response)


def show(request):
    if request.method == 'POST':
        email = request.POST['email']
        name = request.POST['name']
        password = request.POST['password']
        subs_filter = Subscriber.objects.filter(email=email)

        try:
            validate_email(email)
        except Exception:
            return JsonResponse({'message': 'Email is invalid'})
        if len(subs_filter) > 0:
            return JsonResponse({'message': 'Email is already subscribed'})
        if len(name) == 0 or len(name) > 32:
            return JsonResponse({'message': 'Name is not valid'})
        if len(password) < 8:
            return JsonResponse({'message': 'Password is not valid'})
        else:
            return JsonResponse({'message': 'All fields are valid'})
    else:
        return JsonResponse({'message': 'Something is wrong'})


def index(request):
    response['form'] = SubscriberForm()
    return render(request, 'subscribe.html', response)
