function subscribe() {
    $.ajax({
        url: 'subscribe/',
        type: 'POST',
        data: {
            email: $('#id_email').val(),
            name: $('#id_name').val(),
            password: $('#id_password').val(),
            csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').attr('value'),
        },
        success: function(json) {
            $('#validation_status').html("<div class='alert alert-success' data-alert>" + $('#id_name').val() + "'s account is subscribed</div>");
            $('#id_form').children().children('input').val('');
            $('#id_submit').prop('disabled', true);
            alert("Success")
        },
        error: function(xhr, errmsg, err) {
            $('#response_msg').html("<div class='alert alert-danger' data-alert>Error message &times;</div>");
            alert("Error")
        },
    });
}


function validate() {
    $.ajax({
        url: 'validate_email/',
        type: 'POST',
        data: {
            email: $('#id_email').val(),
            name: $('#id_name').val(),
            password: $('#id_password').val(),
            csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').attr('value'),
        },

        success: function(response) {
            if (response.message == "All fields are valid") {
                $('#validation_status').html("<div class='alert alert-success' role='alert'>" + response.message + "</div>");
                $('#id_submit').prop('disabled', false);
            } else {
                $('#validation_status').html("<div class='alert alert-danger' role='alert'>" + response.message + "</div>");
                $('#id_submit').prop('disabled', true);
            }
        },

        error: function(errmsg) {
            console.log(errmsg);
        }
    });
};


$(document).ready(function() {
    var token = $('input[name="csrfmiddlewaretoken"]').attr('value')
    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Csrf-Token', token);
        }
    });
});



$(document).ready(function() {
    var x_timer;
    $('#id_email').keyup(function(e) {
        clearTimeout(x_timer);
        var email = $(this).val();
        x_timer = setTimeout(function() {
            validate();
        }, 100);
    })
});
$(document).ready(function() {
    var x_timer;
    $('#id_name').keyup(function(e) {
        clearTimeout(x_timer);
        var name = $(this).val();
        x_timer = setTimeout(function() {
            validate();
        }, 100);
    })
});
$(document).ready(function() {
    var x_timer;
    $('#id_password').keyup(function(e) {
        clearTimeout(x_timer);
        var password = $(this).val();
        x_timer = setTimeout(function() {
            validate();
        }, 100);
    })
});