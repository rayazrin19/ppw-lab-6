from django import forms
from .models import Status


class Status_Form(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['status']
        widgets = {
            'status': forms.Textarea(attrs={'class': 'form-control'})
        }

    def __init__(self, *args, **kwargs):
        super(Status_Form, self).__init__(*args, **kwargs)
        self.fields['status'].label = "Apa yang sedang kamu pikirkan?"
