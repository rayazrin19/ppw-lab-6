from django.test import TestCase
from django.test import Client
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

from . import views
from .models import Status
from .forms import Status_Form


# Create your tests here.
class Lab6UnitTest(TestCase):

    def test_lab_6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab_6_has_hello_apa_kabar(self):
        request = HttpRequest()
        response = views.index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, apa kabar?</h1>', html_response)

    def test_model_can_create_new_todo(self):
        # Creating a new activity
        new_activity = Status.objects.create(
            status='mengerjakan lab_6 ppw')

        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )

    def test_lab_6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post(
            '/', {'status': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_lab_6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post(
            '/', {'status': ''})
        self.assertEqual(response_post.status_code, 200)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_lab_6_delete_all_has_0_count(self):
        request = HttpRequest()
        response = views.delete_all(request)
        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 0)

    # CHALLENGE
    def test_lab_6_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_lab_6_profile_has_name(self):
        name = 'Ray Azrin Karim'
        request = HttpRequest()
        response = views.profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn(name, html_response)

    def test_lab_6_profile_has_npm(self):
        npm = '1706044111'
        request = HttpRequest()
        response = views.profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn(npm, html_response)


class Lab7FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        super(Lab7FunctionalTest, self).setUp()

    def tearDown(self):
        time.sleep(3)
        self.selenium.quit()
        super(Lab7FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        status = selenium.find_element_by_id('id_status')
        time.sleep(3)
        status.send_keys('Coba Coba')
        status.submit()
        assert "Coba Coba" in self.selenium.page_source

    def test_lab_6_title_is_right(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        title = selenium.find_element_by_id('id_title').text
        assert title in self.selenium.page_source

    def test_lab_6_delete_button_contain_text_delete_all(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        delete = selenium.find_element_by_id('id_delete').text
        assert "Delete All" in delete

    def test_lab_use_background(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        background = selenium.find_element_by_tag_name(
            'body').value_of_css_property('background')
        assert "031.jpg"in background

    def test_lab_title_has_the_right_color(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        title = selenium.find_element_by_id(
            'id_title').value_of_css_property('color')
        self.assertIn('rgba(255, 255, 255, 1)', title)

    def test_lab_8_successfully_change_the_style(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/profile/')
        time.sleep(3)
        background = selenium.find_element_by_tag_name(
            'body').value_of_css_property('background')
        assert "031.jpg"in background

        text = selenium.find_element_by_tag_name(
            'body').value_of_css_property('color')
        self.assertIn('rgba(33, 37, 41, 1)', text)
        ##########
        change = selenium.find_element_by_id('id_change')
        change.click()
        background = selenium.find_element_by_tag_name(
            'body').value_of_css_property('background')
        assert "091.jpg"in background

        text = selenium.find_element_by_tag_name(
            'body').value_of_css_property('color')
        self.assertIn('rgba(33, 37, 41, 1)', text)

        ######
        change.click()
        background = selenium.find_element_by_tag_name(
            'body').value_of_css_property('background')
        assert "031.jpg"in background
