from django.shortcuts import render, redirect
from .forms import Status_Form
from .models import Status

# Create your views here.


def index(request):
    if request.method == 'POST':
        form = Status_Form(request.POST)
        statuses = Status.objects.all().order_by('date')
        if form.is_valid():
            status = Status()
            status.status = form.cleaned_data['status']
            status.save()
            return redirect('lab_6:index')

    else:
        form = Status_Form()
        statuses = Status.objects.all().order_by('date')
    return render(request, 'index.html', {'form': form, 'statuses': statuses})


def delete_all(request):
    Status.objects.all().delete()
    return redirect('lab_6:index')


def profile(request):
    return render(request, 'profile.html')
