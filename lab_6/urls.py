from django.urls import path
from . import views
# url for app
app_name = 'lab_6'
urlpatterns = [
    path('', views.index, name='index'),
    path('delete/', views.delete_all, name='delete'),
    path('profile/', views.profile, name='profile')
]
